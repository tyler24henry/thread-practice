package com.example.backgroundthread.model.remote

interface RandomApi {
    suspend fun getRandomNumber() : Int
}