package com.example.backgroundthread.model

import com.example.backgroundthread.model.remote.RandomApi
import com.example.backgroundthread.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object RandomRepo {
    private val randomApi = object : RandomApi {
        override suspend fun getRandomNumber() : Int {
            return randomColor
        }
    }

    suspend fun getRandomNumber() : Int = withContext(Dispatchers.IO) {
        delay(1000)
        randomApi.getRandomNumber()
    }
}