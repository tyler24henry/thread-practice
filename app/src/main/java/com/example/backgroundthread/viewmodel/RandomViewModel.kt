package com.example.backgroundthread.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.backgroundthread.model.RandomRepo
import kotlinx.coroutines.launch

class RandomViewModel : ViewModel() {
    private val repo = RandomRepo

    private var _randomNumber = MutableLiveData<Int>()
    val randomNumber: LiveData<Int> get() = _randomNumber

    fun getRandomNumber() {
        viewModelScope.launch {
            val randomNumber = repo.getRandomNumber()
            _randomNumber.value = randomNumber
        }
    }
}