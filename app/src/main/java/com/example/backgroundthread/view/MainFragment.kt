package com.example.backgroundthread.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.backgroundthread.databinding.FragmentMainBinding
import com.example.backgroundthread.viewmodel.RandomViewModel
import kotlinx.coroutines.launch

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val randomViewModel by viewModels<RandomViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentMainBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnRandomize.setOnClickListener {
            binding.progress.isVisible = true
            randomViewModel.getRandomNumber()
        }

        randomViewModel.randomNumber.observe(viewLifecycleOwner) { randomNumber ->
            binding.progress.isVisible = false
            val directions = MainFragmentDirections.actionMainFragmentToSecondFragment(randomNumber)
            findNavController().navigate(directions)
        }

//        binding.btnRandomize.setOnClickListener {
//            lifecycleScope.launch {
//                val randomColor: Int = randomViewModel.getRandomNumber()
//                val directions = MainFragmentDirections.actionMainFragmentToSecondFragment(randomColor)
//                findNavController().navigate(directions)
//            }
//        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}




















